let btnChangeColor = document.querySelector(".btnChangeColor");

let darkTheme = localStorage.getItem("darkTheme");
console.log(darkTheme);
if (darkTheme !== null) {
  document.body.classList.add("dark");
}

btnChangeColor.addEventListener("click", function changeColor() {
  darkTheme = localStorage.getItem("darkTheme");
  console.log(darkTheme);

  if (darkTheme !== null) {
    document.body.classList.remove("dark");
    localStorage.removeItem("darkTheme");
  } else {
    localStorage.setItem("darkTheme", 1);
    document.body.classList.add("dark");
  }
});
